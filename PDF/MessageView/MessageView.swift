import UIKit

final class MessageView: UIView {
  @IBOutlet private weak var messageLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()

    isHidden = true
    translatesAutoresizingMaskIntoConstraints = false
    
    layer.cornerRadius = 4.0
    layer.backgroundColor = UIColor.white.cgColor
    layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    layer.shadowRadius = 14.0
    layer.shadowOpacity = 0.28
    layer.shadowColor = UIColor.black.cgColor
  }

  func showMessage(_ message: String, autohide: Bool = true) {
    messageLabel.text = message

    guard isHidden else { return }

    alpha = 0.0
    isHidden = false

    UIView.animate(withDuration: 0.25, animations: {
      self.alpha = 1.0
    }, completion: { _ in
      guard autohide else { return }
      DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [weak self] in
        self?.hideMessage()
      }
    })
  }

  func hideMessage() {
    UIView.animate(withDuration: 0.25, animations: {
      self.alpha = 0.0
    }, completion: { _ in
      self.isHidden = true
    })
  }
}

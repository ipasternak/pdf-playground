//
//  AppDelegate.swift
//  PDF
//
//  Created by Andrey Chevozerov on 16/07/2019.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

import UIKit
import PDFNet

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        PTPDFNet.initialize("demo")
        makePdfCache().map(PTPDFNet.setPersistentCachePath)
        return true
    }
}

private extension AppDelegate {

    func makePdfCache() -> String? {
        guard
            let path = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first,
            let url = URL(string: path)
        else { return nil }

        let cache = url.appendingPathComponent("pdfnet", isDirectory: true).absoluteString

        do {
            let fm = FileManager.default
            if fm.fileExists(atPath: cache) {
                try fm.removeItem(atPath: cache)
            }
            try fm.createDirectory(atPath: cache, withIntermediateDirectories: true, attributes: nil)
        } catch {
            return nil
        }
        return cache
    }
}

import UIKit

final class PDFToolsPanelItem: UIControl {
  static var itemSize = CGSize(width: 42.0, height: 42.0)

  @IBOutlet private var arrowView: UIImageView!
  @IBOutlet private var imageView: UIImageView!
  @IBOutlet private var selectionView: UIView!
  @IBOutlet private var titleLabel: UILabel!

  private (set) var tool: ToolType = .pan {
    didSet { updateAppearance(tool) }
  }

  override var isSelected: Bool {
    didSet { selectionView.isHidden = !isSelected }
  }

  class func item(tool: ToolType) -> PDFToolsPanelItem? {
    guard let object = Bundle.main.loadNibNamed("PDFToolsPanelItem", owner: nil, options: nil)?.first as? PDFToolsPanelItem else { return nil }
    object.tool = tool
    object.updateAppearance(tool)
    return object
  }
}

private extension PDFToolsPanelItem {

  func updateAppearance(_ tool: ToolType) {
    func isSingleTool<T: CaseIterable>(_ type: T.Type) -> Bool {
      return type.allCases.count == 1
    }

    switch tool {
    case .pan:
      titleLabel.text = "Select"

    case .cloud:
      titleLabel.text = "Cloud"

    case .shape(let shape):
      arrowView.isHidden = isSingleTool(ToolType.ShapeType.self)
      switch shape {
      case .rect:
        titleLabel.text = "Shapes"
      }

    case .arrow:
      titleLabel.text = "Arrow"

    case .pen(let pen):
      arrowView.isHidden = isSingleTool(ToolType.PenType.self)
      switch pen {
      case .normal:
        titleLabel.text = "Pen"
      }

    case .highlight(let highlight):
      arrowView.isHidden = isSingleTool(ToolType.HighlightType.self)
      switch highlight {
      case .text:
        titleLabel.text = "Highlight"
      }

    case .text:
      titleLabel.text = "Text"

    case .photo:
      titleLabel.text = "Photo"

    case .measure(let measure):
      arrowView.isHidden = isSingleTool(ToolType.MeasureType.self)
      switch measure {
      case .ruler:
        titleLabel.text = "Measure"
      }

    case .sign:
      titleLabel.text = "Sign"

    case .color:
      titleLabel.text = "Color"
    }

    imageView.image = UIImage.tool(tool)
  }
}

private extension UIImage {
  class func tool(_ tool: ToolType) -> UIImage? {
    let name: String
    switch tool {
    case .pan:        name = "Select"
    case .cloud:      name = "Cloud"
    case .shape:      name = "Shapes"
    case .arrow:      name = "Arrow"
    case .pen:        name = "Pen"
    case .highlight:  name = "Highlight"
    case .text:       name = "Text"
    case .photo:      name = "Photo"
    case .measure:    name = "Measure"
    case .sign:       name = "Sign"
    case .color:      name = "Color"
    }
    return UIImage(named: name)?.withRenderingMode(.alwaysTemplate)
  }
}

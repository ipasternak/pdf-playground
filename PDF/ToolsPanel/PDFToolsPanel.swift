import UIKit
import Tools

enum ToolType {
  enum ShapeType {
    case rect
  }

  enum PenType {
    case normal
  }

  enum HighlightType {
    case text
  }

  enum MeasureType {
    case ruler
  }

  case pan
  case cloud
  case shape(ShapeType)
  case arrow
  case pen(PenType)
  case highlight(HighlightType)
  case text
  case photo
  case measure(MeasureType)
  case sign
  case color(UIColor)
}

final class PDFToolsPanel: UIView {

  weak var toolsManager: PTToolManager?

  var isActive: Bool = false {
    didSet { toggleVisibility(isActive) }
  }

  private var tools: [ToolType] = [
    .pan,
    .shape(.rect),
    .arrow,
    .pen(.normal),
//    .highlight(.text),
    .text,
//    .measure(.ruler),   // TODO: Hidden according to LIMOB-172
  ]

  private var isToolSwitching: Bool = false   // workaround for tools that should be invoked only from Pan (like Text)
  private var currentTool: ToolType = .pan {
    didSet {
      updateSelectedTool(currentTool)
      updateToolManager(currentTool)
    }
  }

  override init(frame: CGRect) {
    super.init(frame: CGRect(x: frame.origin.x,
                             y: frame.origin.y,
                             width: PDFToolsPanelItem.itemSize.width,
                             height: PDFToolsPanelItem.itemSize.height * CGFloat(tools.count)))
    alpha = 0.0
    isHidden = true
    isUserInteractionEnabled = true
    backgroundColor = UIColor.init(white: 51.0 / 255.0, alpha: 0.7)
    layer.cornerRadius = 4.0

    var itemFrame = CGRect(origin: .zero, size: PDFToolsPanelItem.itemSize)
    tools.compactMap(PDFToolsPanelItem.item(tool:)).forEach { item in
      item.addTarget(self, action: #selector(didTapOnTool), for: .touchUpInside)
      item.isEnabled = (item.tool.toolClass != nil)
      item.frame = itemFrame
      itemFrame.origin.y += itemFrame.size.height
      addSubview(item)
    }
    updateSelectedTool(currentTool)
    updateToolManager(currentTool)
  }

  required init?(coder aDecoder: NSCoder) {
    assertionFailure("Coding not supported yet")
    return nil
  }

  override var intrinsicContentSize: CGSize {
    return CGSize(width: PDFToolsPanelItem.itemSize.width, height: PDFToolsPanelItem.itemSize.height * CGFloat(tools.count))
  }
}

extension PDFToolsPanel: PTToolManagerDelegate {
  func toolManagerToolChanged(_ toolManager: PTToolManager) {
    guard
      let toolClass = toolManager.tool?.classForCoder,
      let tool = tools.first(where: { $0.toolClass == toolClass })
    else { return }
    updateSelectedTool(tool)
  }

  func toolManager(_ toolManager: PTToolManager, shouldSwitchTo tool: PTTool) -> Bool {
    return toolManager.tool != tool
  }

  func toolManager(_ toolManager: PTToolManager, shouldSelectAnnotation annotation: PTAnnot, onPageNumber pageNumber: UInt) -> Bool {
    return isActive
  }

  func toolManager(_ toolManager: PTToolManager, handleLongPress gestureRecognizer: UILongPressGestureRecognizer) -> Bool {
    return false
  }

  func toolManager(_ toolManager: PTToolManager, shouldShowMenu menuController: UIMenuController, forAnnotation annotation: PTAnnot?, onPageNumber pageNumber: UInt) -> Bool {
    guard isActive, let annotation = annotation else { return false }
    let type = annotation.getType()
    print("\(#function): \(type)")
    return type != e_ptText
  }
}

private extension PDFToolsPanel {
  func toggleVisibility(_ isVisible: Bool) {
    if isVisible {
      guard alpha < 0.5 else { return }
      isHidden = false
      UIView.animate(withDuration: 0.25) { self.alpha = 1.0 }
    } else {
      guard alpha > 0.5 else { return }
      UIView.animate(withDuration: 0.25, animations: {
        self.alpha = 0
      }, completion: { _ in
        self.currentTool = .pan
        self.isHidden = true
      })
    }
  }

  func updateSelectedTool(_ tool: ToolType) {
    guard !isToolSwitching else { return }
    subviews.compactMap({ $0 as? PDFToolsPanelItem }).forEach({ $0.isSelected = $0.tool == tool })
  }

  func updateToolManager(_ tool: ToolType) {
    defer {
      isToolSwitching = false
    }

    guard let manager = toolsManager, let toolClass = tool.toolClass else { return }

    func switchTool(_ next: ToolType) -> PTTool? {
      guard let toolClass = next.toolClass else { return nil }
      let object = manager.changeTool(toolClass)
      object.backToPanToolAfterUse = false
      return object
    }

    switch tool {
    case .text:
      isToolSwitching = true
      guard let object = switchTool(.pan) else { return assertionFailure() }
      object.nextToolType = toolClass
      manager.createSwitchToolEvent("Start")

    default:
      guard let object = switchTool(tool) else { return assertionFailure() }
      switch object {
      case let freehand as PTFreeHandCreate:
        freehand.multistrokeMode = false
      default:
        break
      }
    }
  }

  @objc func didTapOnTool(_ sender: PDFToolsPanelItem) {
    currentTool = sender.tool
  }
}

extension ToolType: Equatable {}
extension ToolType.PenType: CaseIterable {}
extension ToolType.ShapeType: CaseIterable {}
extension ToolType.MeasureType: CaseIterable {}
extension ToolType.HighlightType: CaseIterable {}

private extension ToolType {
  var toolClass: AnyClass? {
    switch self {
    case .pan:        return PTPanTool.classForCoder()
    case .cloud:      return PTCloudCreate.classForCoder()
    case .shape:      return PTRectangleCreate.classForCoder()
    case .arrow:      return PTArrowCreate.classForCoder()
    case .pen:        return PTFreeHandCreate.classForCoder()
    case .highlight:  return PTTextHighlightCreate.classForCoder()
    case .text:       return PDFTextCreateTool.classForCoder()
    case .photo:      return nil
    case .measure:    return PTRulerCreate.classForCoder()
    case .sign:       return nil
    case .color:      return nil
    }
  }
}

extension PTAnnotType: CustomDebugStringConvertible {
  public var debugDescription: String {
    switch self {
    case e_ptText: return "e_ptText"
    case e_ptLink: return "e_ptLink"
    case e_ptFreeText: return "e_ptFreeText"
    case e_ptLine: return "e_ptLine"
    case e_ptSquare: return "e_ptSquare"
    case e_ptCircle: return "e_ptCircle"
    case e_ptPolygon: return "e_ptPolygon"
    case e_ptPolyline: return "e_ptPolyline"
    case e_ptHighlight: return "e_ptHighlight"
    case e_ptUnderline: return "e_ptUnderline"
    case e_ptSquiggly: return "e_ptSquiggly"
    case e_ptStrikeOut: return "e_ptStrikeOut"
    case e_ptStamp: return "e_ptStamp"
    case e_ptCaret: return "e_ptCaret"
    case e_ptInk: return "e_ptInk"
    case e_ptPopup: return "e_ptPopup"
    case e_ptFileAttachment: return "e_ptFileAttachment"
    case e_ptSound: return "e_ptSound"
    case e_ptMovie: return "e_ptMovie"
    case e_ptWidget: return "e_ptWidget"
    case e_ptScreen: return "e_ptScreen"
    case e_ptPrinterMark: return "e_ptPrinterMark"
    case e_ptTrapNet: return "e_ptTrapNet"
    case e_ptWatermark: return "e_ptWatermark"
    case e_pt3D: return "e_pt3D"
    case e_ptRedact: return "e_ptRedact"
    case e_ptProjection: return "e_ptProjection"
    case e_ptRichMedia: return "e_ptRichMedia"
    case e_ptUnknown: return "e_ptUnknow"
    default: return "something else"
    }
  }
}

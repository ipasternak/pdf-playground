import Tools

final class PDFMarkupsManager: NSObject {
    private weak var docController: PTDocumentViewController?
    private let entityId: String

    private static let kMarkupFolder = "markups"
    private static let kMarkupExtension = "xfdf"

    init(controller: PTDocumentViewController, entityId: String) {
        self.docController = controller
        self.entityId = entityId
    }
}

extension PDFMarkupsManager: PTDocumentViewControllerDelegate {
    func documentViewControllerDidOpenDocument(_ documentViewController: PTDocumentViewController) {
        do {
            try applyAnnotationFiles()
        } catch {
            print(error)
        }
    }
}

extension PDFMarkupsManager: PTToolManagerDelegate {
    func toolManager(_ toolManager: PTToolManager, annotationAdded annotation: PTAnnot, onPageNumber pageNumber: UInt) {
        do {
            try updateAnnotationFile(annotation: annotation, action: .add)
        } catch {
            print("Add annotation failed: \(error)")
        }
    }

    func toolManager(_ toolManager: PTToolManager, annotationRemoved annotation: PTAnnot, onPageNumber pageNumber: UInt) {
        do {
            try removeAnnotationFile(annotation: annotation)
        } catch {
            print(error)
        }
    }

    func toolManager(_ toolManager: PTToolManager, annotationModified annotation: PTAnnot, onPageNumber pageNumber: UInt) {
        do {
            try updateAnnotationFile(annotation: annotation, action: .update)
        } catch {
            print("Update annotation failed: \(error)")
        }
    }
}

private extension PDFMarkupsManager {
    enum PDFError: Error {
        case annotationNameNotSet
        case annotationsNotExtracted
        case documentNotFound
        case markupFileNotFound
        case markupNotSaved
        case wrongMarkupFile
    }

    enum Action {
        case add, update
    }

    func applyAnnotationFiles() throws {
        guard let path = annotationFilesURL() else { return }
        try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: []).forEach(attachAnnotations)
        if let ctrl = docController, let doc = ctrl.pdfViewCtrl.getDoc() {
            try removeDuplicates(pdf: ctrl.pdfViewCtrl, doc: doc)
        }
    }

    func updateAnnotationFile(annotation: PTAnnot, action: Action) throws {
        try createAnnotationsFolder()
        let info = try annotation.getOrCreateInfo(owner: "Megauser", additionalInfo: "test")
//        guard let id = try annotation.getId(assign: true, string: "TestId") else { throw PDFError.markupNotSaved }
        let id = info.id
        guard let path = annotationFileURL(id: id) else { throw PDFError.markupFileNotFound }

        print("Save annotation \(id) to a file: \(path.absoluteString)")
//        switch annotation {
//        case let ink as PTInk:
//            ink.setTitle("Title text")
//            default:
//            break
//        }
        
//         annotation.setContents("Piu")
        try addNewField(annotation: annotation)
        try save(annotation: annotation, url: path, action: action)
        print("Update annotation: \(annotation.humanReadableDescription)")
    }
    
    func addNewField(annotation: PTAnnot) throws {
        guard let controller = docController?.pdfViewCtrl else { throw PDFError.documentNotFound }
        guard let doc = controller.getDoc() else { throw PDFError.documentNotFound }
         try PTPDFNet.catchException {
            controller.docLock(true)
           let obj = doc.createIndirectDict()
            obj?.putDict("Custom")
            obj?.put("test", value: "test")
            annotation.setOptionalContent(obj!)
            annotation.refreshAppearance()
            controller.update()
            controller.docUnlock()
        }
    }
    
    func save(annotation: PTAnnot, url: URL, action: Action, vector: PTVectorAnnot? = nil) throws {
        guard let vector = vector ?? PTVectorAnnot(), let empty = PTVectorAnnot() else {  throw PDFError.markupNotSaved }
        guard let doc = docController?.pdfViewCtrl.getDoc() else { throw PDFError.documentNotFound }
        
        vector.add(annotation)
        var xml: String?
        
        try PTPDFNet.catchException {
                let fdf: PTFDFDoc
                switch action {
                case .add:
                    fdf = doc.fdfExtractCommand(vector, annot_modified: empty, annot_deleted: empty)
                case .update:
                    fdf = doc.fdfExtractCommand(empty, annot_modified: vector, annot_deleted: empty)
                }
                
                //fdf.fieldCreate(with: "test", type: PTFieldType(3), field_value: "Test value")
                xml = fdf.saveAsXFDFToString()
                print("XML file to save: \(xml)")
        }
        try xml?.write(to: url, atomically: true, encoding: .utf8)
    }

    func removeAnnotationFile(annotation: PTAnnot) throws {
        guard let id = try annotation.getId(), let path = annotationFileURL(id: id) else { throw PDFError.annotationNameNotSet }
        try FileManager.default.removeItem(at: path)
    }

    func annotationFilesURL() -> URL? {
        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        return path.appendingPathComponent("\(PDFMarkupsManager.kMarkupFolder)/\(entityId)")
    }

    func annotationFileURL(id: String) -> URL? {
        return annotationFilesURL()?.appendingPathComponent("\(id).\(PDFMarkupsManager.kMarkupExtension)")
    }

    func createAnnotationsFolder() throws {
        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let url = path.appendingPathComponent("\(PDFMarkupsManager.kMarkupFolder)/\(entityId)")
        guard !FileManager.default.fileExists(atPath: url.path) else { return }
        try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
    }

    func attachAnnotations(from url: URL) throws {
        guard let pdf = docController?.pdfViewCtrl else { return }
        guard url.pathExtension == PDFMarkupsManager.kMarkupExtension else { return }
        guard let xml = try? String(contentsOf: url, encoding: .utf8) else { throw PDFError.markupFileNotFound }
        
        print("url: \(url)\n\(xml)\n")
        guard let doc = pdf.getDoc() else { throw PDFError.documentNotFound }
        guard let fdf = PTFDFDoc.create(fromXFDF: xml) else { throw PDFError.wrongMarkupFile }

        print("Load annotation from file: \(url.absoluteString)")

        do {
            try PTPDFNet.catchException {
                pdf.docLock(true)
                doc.fdfMerge(fdf)
                pdf.docUnlock()
            }
        } catch {
            throw error
        }
    }

    func enumerateAnnotations(in pdf: PTPDFViewCtrl, doc: PTPDFDoc, handler: @escaping (PTPage, PTAnnot) -> Void) throws {
        do {
            try PTPDFNet.catchException {
                pdf.docLock(true)
                for i in 0 ..< doc.getPageCount() {
                    guard let page = doc.getPage(UInt32(i)), page.isValid() else { continue }
                    for a in 0 ..< page.getNumAnnots() {
                        guard let annotation = page.getAnnot(UInt32(a)), annotation.isValid() else { continue }
                        handler(page, annotation)
                    }
                }
                pdf.docUnlock()
            }
        } catch {
            throw error
        }
    }

    func removeDuplicates(pdf: PTPDFViewCtrl, doc: PTPDFDoc) throws {
        var annots: [String: PTAnnot] = [:]
        try enumerateAnnotations(in: pdf, doc: doc) { page, annotation in
            guard let custom = annotation.getOptionalContent() else { return }
            print("Custom content: \(custom)\n field: \(custom.get("test"))")
            print("Annotation: \(annotation.humanReadableDescription)")
            guard let date = annotation.getDate(), let id = try? annotation.getId() else { return }
            if let old = annots[id], let oldDate = old.getDate() {
                if oldDate <= date {
                    page.annotRemove(with: old)
                    annots[id] = annotation
                } else {
                    page.annotRemove(with: annotation)
                    pdf.update(with: annotation, page_num: page.getIndex())     // do we need to call this?
                }
            } else {
                annots[id] = annotation
            }
        }
    }
}

struct AnnotationInfo {
    let id: String
    let creator: String
    let additionalInfo: String
    
    init(creator: String, additionalInfo: String = "") {
        id = UUID().uuidString
        self.creator = creator
        self.additionalInfo = additionalInfo
    }
    
    init?(annotation: PTAnnot) {
        guard let obj = annotation.getUniqueID(), obj.isValid(), obj.isString() else { return nil }
        guard let parts = obj.getAsPDFText()?.components(separatedBy: "|"), parts.count == 3 else { return nil }
        
        id = parts[0]
        creator = parts[1]
        additionalInfo = parts[3]
    }
    
    func getString() -> String {
        return "\(id)|\(creator)|\(additionalInfo)"
    }
}

extension PTDate: Comparable {
    public static func < (lhs: PTDate, rhs: PTDate) -> Bool {
        return lhs.compare(rhs) == .orderedAscending
    }
}

extension PTAnnot {
    func getOrCreateInfo(owner: String, additionalInfo: String) throws -> AnnotationInfo {
        if let info = AnnotationInfo(annotation: self) {
            return info
        } else {
            let info = AnnotationInfo(creator: owner, additionalInfo: additionalInfo)
           _ = try assignId(info.getString())
            return info
        }
    }
    
    func getId(assign: Bool = false, string: String? = nil) throws -> String? {
        var id: String?
        do {
            try PTPDFNet.catchException { [weak self] in
                guard
                    let self = self,
                    let obj = self.getUniqueID(),
                    obj.isValid(),
                    obj.isString()
                else { return }
                id = obj.getAsPDFText()
            }
        } catch {
            throw error
        }
        if assign && (id?.isEmpty ?? true) {
            let newId = string ?? UUID().uuidString
            return try assignId(newId)
        } else {
            return id
        }
    }

    func assignId(_ id: String) throws -> String {
        do {
            try PTPDFNet.catchException { [weak self] in
                self?.setUniqueID(id, id_buf_sz: 0)
            }
        } catch {
            throw error
        }
        return id
    }
}

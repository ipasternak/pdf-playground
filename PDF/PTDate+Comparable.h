//
//  PTDate+Comparable.h
//  PDF
//
//  Created by Andrey Chevozerov on 14/08/2019.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

#import <PDFNet/PDFNet.h>

NS_ASSUME_NONNULL_BEGIN

@interface PTDate (Comparable)

- (BOOL)isEqual:(PTDate *)object;
- (NSComparisonResult)compare:(PTDate *)object;

@end

NS_ASSUME_NONNULL_END

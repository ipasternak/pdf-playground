import Tools

final class PDFDocumentViewController: PTDocumentViewController {
  var backButtonPressed: () -> Void = {}
  var moreButtonPressed: () -> Void = {}
  var recentButtonPressed: () -> Void = {}
  var bookmarkButtonPressed: (Int32) -> Void = { _ in }

  private let entityId: String
  private var pdfDelegateProxy = PDFDocumentDelegate()

  private var navigationBarPanel: PDFNavigationBarPanel?
  private var markupsManager: PDFMarkupsManager?
  private var toolsPanelView: PDFToolsPanel?
  private var textFinder: PDFTextFinder?

  private var initialPage: Int32 = 0
  private var isRendered: Bool = false
  private var bookmarkedPages: [Int32] = []
  private var highlightedTexts: [String] = []

  private weak var titleLabel: UILabel?
  private weak var messageView: MessageView?

  init(entityId: String) {
    self.entityId = entityId
    super.init()
    print("TEST: \(self) init")
  }

    deinit {
        print("TEST: \(self) deinit")
    }
    
  override func viewDidLoad() {
    super.viewDidLoad()

    delegate = pdfDelegateProxy
    toolManager.delegate = pdfDelegateProxy

    pdfViewCtrl.alpha = 0.0   // do not show content until PDF has been rendered for the first time

    automaticallyHidesControls = false
    adjustStyle()
    setupAdditionalControls()
    setupAnnotationsManager()
    navigationBarPanel = PDFNavigationBarPanel(navigationItem: navigationItem, didTapAction: { [weak self] action, item in
        self?.handleNavigationPanelAction(action, item)
    })
  }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // closeDocument()
    }

  func showBookmarkMessage(_ message: String?) {
    guard let message = message, !message.isEmpty, let panel = messageView else { return }
    view.bringSubviewToFront(panel)
    panel.showMessage(message)
  }

  func updateDocumentUrl(_ urlString: String) {
    guard let url = cleanUrl(urlString) else {
       // closeDocument()
        return
    }
    openDocument(with: url)
  }

  func updateDocumentTitle(_ title: String?) {
    titleLabel?.text = title
  }

  func updateCurrentPage(_ currentPage: Int32) {
    if isRendered {
      pdfViewCtrl.setCurrentPage(currentPage)
    } else {
      initialPage = currentPage
    }
  }

  func updateHighlights(_ highlightedTexts: [String]) {
    self.highlightedTexts = highlightedTexts
    findAndHighlightTexts()
  }

  func updateBookmarks(_ bookmarkedPages: [Int32]) {
    self.bookmarkedPages = bookmarkedPages
    updateBookmarkState()
  }

  // MARK: - PTPDFViewCtrlDelegate overrides

  override func pdfViewCtrl(_ pdfViewCtrl: PTPDFViewCtrl, onSetDoc doc: PTPDFDoc) {
    super.pdfViewCtrl(pdfViewCtrl, onSetDoc: doc)

    textFinder = PDFTextFinder(document: doc)
    findAndHighlightTexts()
  }

  override func pdfViewCtrl(onRenderFinished pdfViewCtrl: PTPDFViewCtrl) {
    super.pdfViewCtrl(onRenderFinished: pdfViewCtrl)

    guard !isRendered else { return }

    isRendered = true

    let block = { [weak self] () -> Void in
      self?.pdfViewCtrl.alpha = 1
    }
    if initialPage == pdfViewCtrl.currentPage {
      block()
    } else {
      pdfViewCtrl.setCurrentPage(initialPage)
      UIView.animate(withDuration: 0.3, animations: block)
      updateBookmarkState()
    }
  }

  override func pdfViewCtrl(_ pdfViewCtrl: PTPDFViewCtrl, pageNumberChangedFrom oldPageNumber: Int32, to newPageNumber: Int32) {
    super.pdfViewCtrl(pdfViewCtrl, pageNumberChangedFrom: oldPageNumber, to: newPageNumber)

    updateBookmarkState()
  }
}

private extension PDFDocumentViewController {
  var isBookmarked: Bool {
    return bookmarkedPages.contains(pdfViewCtrl.currentPage)
  }

  func updateBookmarkState() {
    guard let panel = navigationBarPanel else { return }
    if case .normal(let wasBookmarked) = panel.mode, wasBookmarked != isBookmarked {
      panel.changeMode(.normal(isBookmarked: isBookmarked))
    }
  }

  func handleNavigationPanelAction(_ action: PDFNavigationBarPanel.Action, _ sender: UIBarButtonItem?) {
    guard let panel = navigationBarPanel else { return }

    switch action {
    case .back:
      backButtonPressed()

    case .done:
      toolsPanelView?.isActive = false
      panel.changeMode(.normal(isBookmarked: isBookmarked))

    case .markup:
      guard let toolbar = toolsPanelView else { return }
      toolbar.isActive = true
      panel.changeMode(.markup(canUndo: false, canRedo: false))

    case .chooseBinder:
      break

    case .undo:
      break

    case .redo:
      break

    case .search:
      perform(searchButtonItem.action, with: sender)

    case .recent:
      recentButtonPressed()

    case .bookmark:
      bookmarkButtonPressed(pdfViewCtrl.currentPage)

    case .viewMode:
      break

    case .more:
      moreButtonPressed()
    }
  }

  func findAndHighlightTexts() {
    guard !highlightedTexts.isEmpty else { return }
    textFinder?.findAndAnnotateTexts(highlightedTexts)
  }

  func findActivityIndicatorView() -> UIActivityIndicatorView? {
    return view.subviews.compactMap({ $0 as? UIActivityIndicatorView }).first
  }

  func adjustStyle() {
    navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    findActivityIndicatorView()?.color = UIColor.black
    thumbnailSliderController.view.tintColor = UIColor.black
    isNavigationListsButtonHidden = true

    let backgroundColor = UIColor(white: 239.0 / 256.0, alpha: 1.0)
    view.backgroundColor = backgroundColor
    pdfViewCtrl.backgroundColor = backgroundColor
  }

  func setupAnnotationsManager() {
    let manager = PDFMarkupsManager(controller: self, entityId: entityId)
    pdfDelegateProxy.addDelegateSubscription(manager)
    pdfDelegateProxy.addToolManagerSubscription(manager)
    markupsManager = manager
  }

  func setupAdditionalControls() {
    titleLabel = PDFDocumentViewBuilder.createTitleLabel(in: view)
    messageView = PDFDocumentViewBuilder.createMessageView(in: view)
    toolsPanelView = PDFDocumentViewBuilder.createToolbarPanel(in: view, toolManager: toolManager, delegateProxy: pdfDelegateProxy)
  }

  func cleanUrl(_ str: String) -> URL? {
    if let url = URL(string: str) {
      return url;
    }
    if let urlEscapedString = str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let escapedURL = URL(string: urlEscapedString) {
      return escapedURL
    }
    return nil
  }
}

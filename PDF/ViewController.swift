//
//  ViewController.swift
//  PDF
//
//  Created by Andrey Chevozerov on 16/07/2019.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {
}

private extension ViewController {
    @IBAction func openPdf(_ sender: UIButton) {
        guard let path = Bundle.main.url(forResource: "Example", withExtension: "pdf"), let nc = navigationController else { return }

        let controller = PDFDocumentViewController(entityId: "example")
        controller.backButtonPressed = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        controller.updateDocumentUrl(path.absoluteString)
        controller.updateDocumentTitle("Example")
        nc.pushViewController(controller, animated: true)
    }
}

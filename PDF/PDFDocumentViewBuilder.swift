import UIKit
import Tools

enum PDFDocumentViewBuilder {

  static func createActivityIndicator(in view: UIView) -> UIActivityIndicatorView {
    let indicatorView = UIActivityIndicatorView(style: .whiteLarge)
    indicatorView.color = UIColor.black
    indicatorView.hidesWhenStopped = true
    indicatorView.center = view.center
    indicatorView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(indicatorView)

    NSLayoutConstraint.activate([
      indicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      indicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])
    return indicatorView
  }

  static func createTitleLabel(in view: UIView) -> UILabel {
    let label = UILabel()
    label.font = UIFont(name: "HelveticaNeue", size: 12)
    label.textColor = UIColor.black
    label.textAlignment = .center
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(label)

    let margins = view.layoutMarginsGuide
    NSLayoutConstraint.activate([
      label.topAnchor.constraint(equalTo: margins.topAnchor, constant: 8.0),
      label.leadingAnchor.constraint(greaterThanOrEqualTo: margins.leadingAnchor, constant: 16.0),
      label.trailingAnchor.constraint(greaterThanOrEqualTo: margins.trailingAnchor, constant: 16.0),
      label.centerXAnchor.constraint(equalTo: view.centerXAnchor)
    ])
    return label
  }

  static func createMessageView(in view: UIView) -> MessageView? {
    guard let object = Bundle.main.loadNibNamed("MessageView", owner: nil, options: nil)?.first as? MessageView else { return nil }

    view.addSubview(object)

    NSLayoutConstraint.activate([
      object.widthAnchor.constraint(greaterThanOrEqualToConstant: 250.0),
      object.widthAnchor.constraint(lessThanOrEqualToConstant: view.bounds.size.width - 40.0),
      object.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      object.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])
    return object
  }

  static func createToolbarPanel(in view: UIView, toolManager: PTToolManager, delegateProxy: PDFDocumentDelegate) -> PDFToolsPanel {
    let toolbarView = PDFToolsPanel()
    toolbarView.translatesAutoresizingMaskIntoConstraints = false
    toolbarView.toolsManager = toolManager
    delegateProxy.setMasterToolManager(toolbarView)
    view.addSubview(toolbarView)

    NSLayoutConstraint.activate([
      toolbarView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor, constant: 10.0),
      toolbarView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
    ])
    return toolbarView
  }

}

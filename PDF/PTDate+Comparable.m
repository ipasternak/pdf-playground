//
//  PTDate+Comparable.m
//  PDF
//
//  Created by Andrey Chevozerov on 14/08/2019.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

#import "PTDate+Comparable.h"

@implementation PTDate (Comparable)

- (BOOL)isEqual:(PTDate *)object {
    return [self getYear] == [object getYear] &&
        [self getMonth] == [object getMonth] &&
        [self getDay] == [object getDay] &&
        [self getHour] == [object getHour] &&
        [self getMinute] == [object getMinute] &&
        [self getSecond] == [object getSecond];
}

- (NSComparisonResult)compare:(PTDate *)object {
    UInt16 year1 = [self getYear];
    UInt16 year2 = [object getYear];
    UInt8 month1 = [self getMonth];
    UInt8 month2 = [object getMonth];
    UInt8 day1 = [self getDay];
    UInt8 day2 = [object getDay];
    UInt8 hour1 = [self getHour];
    UInt8 hour2 = [object getHour];
    UInt8 minute1 = [self getMinute];
    UInt8 minute2 = [object getMinute];
    UInt8 second1 = [self getSecond];
    UInt8 second2 = [object getSecond];

    if (year1 == year2) {
        if (month1 == month2) {
            if (day1 == day2) {
                if (hour1 == hour2) {
                    if (minute1 == minute2) {
                        if (second1 == second2) {
                            return NSOrderedSame;
                        }
                        return second1 < second2 ? NSOrderedAscending : NSOrderedDescending;
                    }
                    return minute1 < minute2 ? NSOrderedAscending : NSOrderedDescending;
                }
                return hour1 < hour2 ? NSOrderedAscending : NSOrderedDescending;
            }
            return day1 < day2 ? NSOrderedAscending : NSOrderedDescending;
        }
        return month1 < month2 ? NSOrderedAscending : NSOrderedDescending;
    }
    return year1 < year2 ? NSOrderedAscending : NSOrderedDescending;
}

@end

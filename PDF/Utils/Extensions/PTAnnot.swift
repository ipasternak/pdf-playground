//
//  PTAnnotation.swift
//  PDF
//
//  Created by Iaroslav Pasternak on 9/18/19.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

import Foundation
import PDFKit

extension PTAnnot: HumanReadable {
    var humanReadableDescription: String {
        let additionalInfo = getOptionalContent()?.getName() ?? "none"
        let contents = getContents() ?? "none"
        return "\(self), type: \(getType().humanReadableDescription), contents: \(contents) additionalInfo: \(additionalInfo)"
    }
}

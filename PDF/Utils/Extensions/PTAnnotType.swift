//
//  PTAnnotType.swift
//  PDF
//
//  Created by Iaroslav Pasternak on 9/18/19.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

import Foundation
import PDFKit

extension PTAnnotType: HumanReadable {
    var humanReadableDescription: String {
        let result: String
        switch self {
        case e_ptText: result = "Text"
        case e_ptLink: result = "Link"
        case e_ptFreeText: result = "Free text"
        case e_ptLine: result = "Line"
        case e_ptSquare: result = "Square"
        case e_ptCircle: result = "Circle"
        case e_ptPolygon: result = "Polygon"
        case e_ptPolyline: result = "Polyline"
        case e_ptHighlight: result = "Highlight"
        case e_ptUnderline: result = "Underline"
        case e_ptSquiggly: result = "Squiggly"
        case e_ptStrikeOut: result = "Strike out"
        case e_ptStamp: result = "Stamp"
        case e_ptCaret: result = "Caret"
        case e_ptInk: result = "Ink"
        case e_ptPopup: result = "Popup"
        case e_ptFileAttachment: result = "File attachment"
        case e_ptSound: result = "Sound"
        case e_ptMovie: result = "Movie"
        case e_ptWidget: result = "Widget"
        case e_ptScreen: result = "Screen"
        case e_ptPrinterMark: result = "Printer mark"
        case e_ptTrapNet: result = "Trap net"
        case e_ptWatermark: result = "Watermark"
        case e_pt3D: result = "3D"
        case e_ptRedact: result = "Redact"
        case e_ptProjection: result = "Projection"
        case e_ptRichMedia: result = "Rich media"
        case e_ptUnknown: result = "Unknown"
        default: result = "Undefined value \(rawValue)"
        }
        return result
    }
}

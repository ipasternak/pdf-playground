//
//  HumanReadable.swift
//  PDF
//
//  Created by Iaroslav Pasternak on 9/18/19.
//  Copyright © 2019 Grid Dynamics. All rights reserved.
//

import Foundation

protocol HumanReadable {
    var humanReadableDescription: String { get }
}

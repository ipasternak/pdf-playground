import Foundation
import PDFNet

class PDFTextFinder {
  private let document: PTPDFDoc
  
  init(document: PTPDFDoc) {
    self.document = document
  }
  
  func findAndAnnotateTexts(_ texts: [String]) {
    let textSearch = PTTextSearch()!
    let mode = e_ptreg_expression.rawValue | e_ptwhole_word.rawValue | e_pthighlight.rawValue
    
    let pattern = "\(texts.joined(separator: "|"))"
    textSearch.begin(document, pattern: pattern, mode: mode, start_page: -1, end_page: -1)
    while true {
      let result = textSearch.run()!
      if result.isFound() {
        let hlts: PTHighlights = result.getHighlights()
        hlts.begin(document)
        while hlts.hasNext() {
          let currentPage: PTPage = document.getPage(UInt32(hlts.getCurrentPageNumber()))
          let quads: PTVectorQuadPoint = hlts.getCurrentQuads()
          var i: Int = 0

          while i < quads.size() {
            //assume each quad is an axis-aligned rectangle
            let q: PTQuadPoint = quads.get(Int32(i))
            let x1: Double = min(min(min(q.getP1().getX(), q.getP2().getX()), q.getP3().getX()), q.getP4().getX())
            let x2: Double = max(max(max(q.getP1().getX(), q.getP2().getX()), q.getP3().getX()), q.getP4().getX())
            let y1: Double = min(min(min(q.getP1().getY(), q.getP2().getY()), q.getP3().getY()), q.getP4().getY())
            let y2: Double = max(max(max(q.getP1().getY(), q.getP2().getY()), q.getP3().getY()), q.getP4().getY())
            let rect = PTPDFRect(x1: x1, y1: y1, x2: x2, y2: y2)
            let annot = PTHighlightAnnot.create(document.getSDFDoc(), pos: rect)!
            annot.setColor(PTColorPt(x: 1, y: 1, z: 0, w: 0), numcomp: 3)
            currentPage.annotPushBack(annot)
            i += 1
          }
          hlts.next()
        }
      } else {
        break
      }
    }
  }
}

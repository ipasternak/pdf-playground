import Foundation
import Tools

class WeakBox<T: AnyObject> {
    weak var item: T?
    
    init(item: T) {
        self.item = item
    }
}

final class PDFDocumentDelegate: NSObject {
  private weak var masterDelegate: PTDocumentViewControllerDelegate?
  private var delegateSubscribers: [WeakBox<PTDocumentViewControllerDelegate>] = []
  private weak var masterToolManager: PTToolManagerDelegate?
  private var toolManagerSubscribers: [WeakBox<PTToolManagerDelegate>] = []

  func setMasterDelegate(_ master: PTDocumentViewControllerDelegate) {
    masterDelegate = master
    addDelegateSubscription(master)
  }

  func addDelegateSubscription(_ subscriber: PTDocumentViewControllerDelegate) {
    let box = WeakBox(item: subscriber)
    delegateSubscribers.append(box)
  }

  func setMasterToolManager(_ master: PTToolManagerDelegate) {
    masterToolManager = master
    addToolManagerSubscription(master)
  }

  func addToolManagerSubscription(_ subscriber: PTToolManagerDelegate) {
    let box = WeakBox(item: subscriber)
    toolManagerSubscribers.append(box)
  }
    
//    func removeSubscriber(_ subscriber: Any) {
//        if let subscriber = subscriber as? PTDocumentViewControllerDelegate {
//            delegateSubscribers.removeAll { $0 === subscriber }
//        }
//        if let subscriber = subscriber as? PTToolManagerDelegate {
//            toolManagerSubscribers.removeAll { $0 === subscriber }
//        }
//    }
}

extension PDFDocumentDelegate: PTDocumentViewControllerDelegate {
  func documentViewControllerDidBecomeInvalid(_ documentViewController: PTDocumentViewController) {
    delegateSubscribers.forEach { $0.item?.documentViewControllerDidBecomeInvalid?(documentViewController) }
  }

  func documentViewControllerDidOpenDocument(_ documentViewController: PTDocumentViewController) {
    delegateSubscribers.forEach { $0.item?.documentViewControllerDidOpenDocument?(documentViewController) }
  }

  func documentViewController(_ documentViewController: PTDocumentViewController, didFailToOpenDocumentWithError error: Error) {
    delegateSubscribers.forEach { $0.item?.documentViewController?(documentViewController, didFailToOpenDocumentWithError: error) }
  }

  func documentViewController(_ documentViewController: PTDocumentViewController, destinationURLForDocumentAt sourceUrl: URL) -> URL? {
    return masterDelegate?.documentViewController?(documentViewController, destinationURLForDocumentAt: sourceUrl)
  }

  func documentViewController(_ documentViewController: PTDocumentViewController, shouldExportCachedDocumentAt cachedDocumentUrl: URL) -> Bool {
    return masterDelegate?.documentViewController?(documentViewController, shouldExportCachedDocumentAt: cachedDocumentUrl) ?? true
  }

  func documentViewController(_ documentViewController: PTDocumentViewController, shouldDeleteCachedDocumentAt cachedDocumentUrl: URL) -> Bool {
    return masterDelegate?.documentViewController?(documentViewController, shouldDeleteCachedDocumentAt: cachedDocumentUrl) ?? true
  }
}

extension PDFDocumentDelegate: PTToolManagerDelegate {
  func toolManagerToolChanged(_ toolManager: PTToolManager) {
    toolManagerSubscribers.forEach { $0.item?.toolManagerToolChanged?(toolManager) }
  }

  func toolManager(_ toolManager: PTToolManager, handle fileAttachment: PTFileAttachment, onPageNumber pageNumber: UInt) {
    toolManagerSubscribers.forEach { $0.item?.toolManager?(toolManager, handle: fileAttachment, onPageNumber: pageNumber) }
  }

  func toolManager(_ toolManager: PTToolManager, didSelectAnnotation annotation: PTAnnot, onPageNumber pageNumber: UInt) {
    toolManagerSubscribers.forEach { $0.item?.toolManager?(toolManager, didSelectAnnotation: annotation, onPageNumber: pageNumber) }
  }

  func toolManager(_ toolManager: PTToolManager, annotationAdded annotation: PTAnnot, onPageNumber pageNumber: UInt) {
    toolManagerSubscribers.forEach { $0.item?.toolManager?(toolManager, annotationAdded: annotation, onPageNumber: pageNumber) }
  }

  func toolManager(_ toolManager: PTToolManager, annotationRemoved annotation: PTAnnot, onPageNumber pageNumber: UInt) {
    toolManagerSubscribers.forEach { $0.item?.toolManager?(toolManager, annotationRemoved: annotation, onPageNumber: pageNumber) }
  }

  func toolManager(_ toolManager: PTToolManager, annotationModified annotation: PTAnnot, onPageNumber pageNumber: UInt) {
    toolManagerSubscribers.forEach { $0.item?.toolManager?(toolManager, annotationModified: annotation, onPageNumber: pageNumber) }
  }

  func toolManager(_ toolManager: PTToolManager, formFieldDataModified annotation: PTAnnot, onPageNumber pageNumber: UInt) {
    toolManagerSubscribers.forEach { $0.item?.toolManager?(toolManager, formFieldDataModified: annotation, onPageNumber: pageNumber) }
  }

  func toolManager(_ toolManager: PTToolManager, shouldSwitchTo tool: PTTool) -> Bool {
    return masterToolManager?.toolManager?(toolManager, shouldSwitchTo: tool) ?? true
  }

  func toolManager(_ toolManager: PTToolManager, shouldHandleLinkAnnotation annotation: PTAnnot?, orLinkInfo linkInfo: PTLinkInfo?, onPageNumber pageNumber: UInt) -> Bool {
    return masterToolManager?.toolManager?(toolManager, shouldHandleLinkAnnotation: annotation, orLinkInfo: linkInfo, onPageNumber: pageNumber) ?? true
  }

  func toolManager(_ toolManager: PTToolManager, shouldShowMenu menuController: UIMenuController, forAnnotation annotation: PTAnnot?, onPageNumber pageNumber: UInt) -> Bool {
    return masterToolManager?.toolManager?(toolManager, shouldShowMenu: menuController, forAnnotation: annotation, onPageNumber: pageNumber) ?? true
  }

  func toolManager(_ toolManager: PTToolManager, shouldSelectAnnotation annotation: PTAnnot, onPageNumber pageNumber: UInt) -> Bool {
    return masterToolManager?.toolManager?(toolManager, shouldSelectAnnotation: annotation, onPageNumber: pageNumber) ?? true
  }

  func toolManager(_ toolManager: PTToolManager, handleTap gestureRecognizer: UITapGestureRecognizer) -> Bool {
    return masterToolManager?.toolManager?(toolManager, handleTap: gestureRecognizer) ?? false
  }

  func toolManager(_ toolManager: PTToolManager, handleDoubleTap gestureRecognizer: UITapGestureRecognizer) -> Bool {
    return masterToolManager?.toolManager?(toolManager, handleDoubleTap: gestureRecognizer) ?? false
  }

  func toolManager(_ toolManager: PTToolManager, handleLongPress gestureRecognizer: UILongPressGestureRecognizer) -> Bool {
    return masterToolManager?.toolManager?(toolManager, handleLongPress: gestureRecognizer) ?? false
  }
}

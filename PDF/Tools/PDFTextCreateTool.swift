//
//  PDFTextCreateTool.swift
//  linq
//
//  Created by Andrey Chevozerov on 04/07/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Tools

final class PDFTextCreateTool: PTCreateToolBase {
  private var isRTL: Bool = false
  private var isDrag: Bool = false
  private var isCreated: Bool = false
  private var isWritten: Bool = false
  private var isKeyboardShown: Bool = false

  private var textView: UITextView?
  private var scrollView: UIScrollView?
  private var touchesEndedTime: Date = Date()

  private var writeRect: PTPDFRect?
  private var writePage: PTPage?

  // MARK: -

  required init(pdfViewCtrl in_pdfViewCtrl: PTPDFViewCtrl) {
    super.init(pdfViewCtrl: in_pdfViewCtrl)
    backgroundColor = UIColor.clear

    let textview = UITextView(frame: .zero)
    in_pdfViewCtrl.addSubview(textview)
    textview.becomeFirstResponder()

    if let language = textview.textInputMode?.primaryLanguage {
      isRTL = NSLocale.characterDirection(forLanguage: language) == .rightToLeft
    }

    textview.resignFirstResponder()
    textview.removeFromSuperview()

    let nc = NotificationCenter.default
    nc.addObserver(self, selector: #selector(inputModeChanged), name: UITextInputMode.currentInputModeDidChangeNotification, object: nil)
    nc.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    nc.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  override var annotClass: AnyClass { return PTFreeText.classForCoder() }
  override var annotType: PTExtendedAnnotType { return .freeText }
  override var canEditStyle: Bool { return true }
  override var createsAnnotation: Bool { return true }

  // MARK: - PDF View Controller overrides

  override func pdfViewCtrl(_ pdfViewCtrl: PTPDFViewCtrl, handleTap gestureRecognizer: UITapGestureRecognizer) -> Bool {
    print(#function)

    if !isCreated {
      isDrag = false
      isCreated = createEntry(point: gestureRecognizer.location(ofTouch: 0, in: pdfViewCtrl))
      return true
    }

    if let tv = textView {
      if isDrag && Date().timeIntervalSince(touchesEndedTime) < 0.85 {
        return true
      }
      tv.resignFirstResponder()
      frame = .zero
    }

    if backToPanToolAfterUse {
      nextToolType = PTPanTool.classForCoder()
      return false
    }

    isCreated = false
    return true
  }

  override func pdfViewCtrl(_ pdfViewCtrl: PTPDFViewCtrl, onTouchesMoved touches: Set<UITouch>, with event: UIEvent?) -> Bool {
    isDrag = true
    touchesEndedTime = Date()
    return true
  }

  override func pdfViewCtrl(_ pdfViewCtrl: PTPDFViewCtrl, onTouchesEnded touches: Set<UITouch>, with event: UIEvent?) -> Bool {
    guard isDrag, let touch = touches.first else { return true }

    if !isCreated {
      isCreated = createEntry(point: touch.location(in: pdfViewCtrl))
    } else if backToPanToolAfterUse {
      nextToolType = PTPanTool.classForCoder()
      return false
    }
    return true
  }

  override func pdfViewCtrl(_ pdfViewCtrl: PTPDFViewCtrl, onTouchesCancelled touches: Set<UITouch>, with event: UIEvent?) -> Bool {
    return true
  }

  override func onSwitchToolEvent(_ userData: Any?) -> Bool {
    guard let event = userData as? String else { return false }

    print("\(#function): \(event)")

    switch event {
    case "Start":
      isCreated = false
      startPoint = longPressPoint
      setTextAreaFrame()
      setupTextEntry()
      activateTextEntry()
      return true

    case "BackToPan":
      if !backToPanToolAfterUse {
        nextToolType = PTPanTool.classForCoder()
        return false
      }

    case "CloseAnnotationToolbar":
      if !backToPanToolAfterUse {
        return false
      }

    default:
      print("[\(type(of: self))] Unknown event: \(event)")
    }

    print("[\(type(of: self))] Unhandled case")
    return false
  }

  override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }
}

extension PDFTextCreateTool: UITextViewDelegate {
  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    return true
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    isDrag = false
    writeAnnotation()
  }
}

private extension PDFTextCreateTool {
  // MARK: - Obj-C area

  @objc func inputModeChanged(_ notification: Notification) {
    guard let tv = textView, !tv.text.isEmpty, let lang = tv.textInputMode?.primaryLanguage else { return }
    let prev = isRTL
    isRTL = NSLocale.characterDirection(forLanguage: lang) == .rightToLeft
    if prev != isRTL {
      setTextAreaFrame()
    }
  }

  @objc func keyboardWillShow(_ notification: Notification) {
    guard let pdf = pdfViewCtrl, let tv = textView else { return }

    let cursorRect = tv.caretRect(for: tv.selectedTextRange!.start)
    let screenRect = pdf.convert(cursorRect, from: tv)
    var topEdge: CGFloat = 0.0
    if #available(iOS 11.0, *) {
      topEdge = pdf.safeAreaInsets.top
    }
    pdf.keyboardWillShow(notification, rectToNotOverlapWith: screenRect, topEdge: topEdge)
    isKeyboardShown = true
  }

  @objc func keyboardWillHide(_ notification: Notification) {
    isCreated = false
    isKeyboardShown = false
    pdfViewCtrl?.keyboardWillHide(notification)
    toolManager?.createSwitchToolEvent("BackToPan")
  }

  @objc func textTap(_ recognizer: UITapGestureRecognizer) {
    guard let tv = textView, let pdf = pdfViewCtrl else { return }
    let textSize = (tv.text as NSString).boundingRect(with: tv.bounds.size,
                                                      options: [.usesLineFragmentOrigin, .usesFontLeading],
                                                      attributes: [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: CGFloat(PTColorDefaults.defaultFreeTextSize() * pdf.zoom))!],
                                                      context: nil).size
    let down = recognizer.location(in: tv)

    if down.x - 6.0 < textSize.width && down.y - 6.0 < textSize.height {
      // do nothing
    } else {
      _ = pdfViewCtrl(pdf, handleTap: recognizer)
    }
  }

  // MARK: - Private area

  class func setRect(freeText: PTFreeText, rect: PTPDFRect, pdfViewCtrl: PTPDFViewCtrl, isRTL: Bool) {
    print(#function)
  }

  func createEntry(point: CGPoint) -> Bool {
    print(#function)

    assert(Thread.isMainThread)

    superview?.bringSubviewToFront(self)
    startPoint = point
    longPressPoint = point

    if !setTextAreaFrame() {
      nextToolType = PTPanTool.classForCoder()
      return false
    }

    if textView == nil && scrollView == nil {
      setupTextEntry()
    }
//    activateTextEntry()
    return true
  }

  @discardableResult
  func setTextAreaFrame() -> Bool {
    guard let pdf = pdfViewCtrl, let doc = pdf.getDoc() else { return false }

    let pageNumber = pdf.getPageNumber(fromScreenPt: Double(startPoint.x), y: Double(startPoint.y))
    if pageNumber < 1 {
      return false
    }

    guard let testRect = doc.getPage(UInt32(pageNumber))?.getCropBox() else { return false }

    let cb1 = convertPagePt(toScreenPt: CGPoint(x: testRect.getX1(), y: testRect.getY1()), onPageNumber: pageNumber)
    let cb2 = convertPagePt(toScreenPt: CGPoint(x: testRect.getX2(), y: testRect.getY2()), onPageNumber: pageNumber)

    let pageLeft = min(cb1.x, cb2.x)
    let pageTop = min(cb1.y, cb2.y)

    let offset = CGFloat(PTColorDefaults.defaultBorderThickness(for: .freeText) * pdf.zoom)

    if isRTL {
      endPoint.x = startPoint.x + offset
      endPoint.y = max(cb1.y, cb2.y)
      startPoint.x = pageLeft
      startPoint.y = max(pageTop, startPoint.y + offset)
    } else {
      endPoint.x = max(cb1.x, cb2.x)
      endPoint.y = max(cb1.y, cb2.y)
      startPoint.x = max(pageLeft, startPoint.x - offset)
      startPoint.y = max(pageTop, startPoint.y - offset)
    }

    frame = CGRect(x: pdf.getHScrollPos() + Double(startPoint.x),
                   y: pdf.getVScrollPos() + Double(startPoint.y),
                   width: Double(abs(endPoint.x - startPoint.x)),
                   height: Double(abs(endPoint.y - startPoint.y)))
    return true
  }

  func setupTextEntry() {
    guard let pdf = pdfViewCtrl else { return }

    isUserInteractionEnabled = true

    let tv = UITextView()
    tv.contentInset = .zero
    tv.textContainerInset = .zero
    tv.textContainer.lineFragmentPadding = 0.0
    tv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    tv.isUserInteractionEnabled = true
    tv.delegate = self
    tv.backgroundColor = UIColor.clear
    tv.textColor = PTColorDefaults.defaultColor(for: .freeText, attribute: ATTRIBUTE_TEXT_COLOR, colorPostProcessMode: pdf.colorPostProcessMode)
    tv.font = UIFont(name: "Helvetica", size: CGFloat(PTColorDefaults.defaultFreeTextSize() * pdf.zoom))

    let recognizer = UITapGestureRecognizer(target: self, action: #selector(textTap))
    recognizer.delegate = self
    recognizer.cancelsTouchesInView = true
    recognizer.delaysTouchesEnded = false
    tv.addGestureRecognizer(recognizer)

    let sv = UIScrollView(frame: frame)
    sv.isUserInteractionEnabled = true
    sv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    sv.backgroundColor = UIColor.red
    sv.addSubview(tv)

//    addSubview(sv)
    textView = tv
    scrollView = sv

    let view = UIView(frame: frame)
    view.backgroundColor = UIColor.red
    addSubview(view)

    isCreated = false
    isWritten = false
  }

  func activateTextEntry() {
    guard let tv = textView, let sv = scrollView, let pdf = pdfViewCtrl, let doc = pdf.getDoc() else { return }

    let cursorRect = tv.caretRect(for: tv.selectedTextRange!.start)
    let thickness = PTColorDefaults.defaultBorderThickness(for: .freeText) * pdf.zoom
    let inset = CGFloat(thickness * 2.0)
    tv.textContainerInset = UIEdgeInsets(top: -inset,
                                         left: -inset - cursorRect.size.height / 20.0,
                                         bottom: frame.size.width - inset,
                                         right: frame.size.height - inset - cursorRect.size.height / 2.0)
    tv.frame = CGRect(origin: .zero, size: sv.frame.size)

    if tv.canBecomeFirstResponder {
      tv.becomeFirstResponder()
    }

    isCreated = true

    pdf.docLock(true)

    var pageNumber = pdf.getPageNumber(fromScreenPt: Double(startPoint.x), y: Double(startPoint.y))
    if pageNumber < 1 {
      pageNumber = pdf.getPageNumber(fromScreenPt: Double(endPoint.x), y: Double(endPoint.y))
    }
    if pageNumber < 1 {
      nextToolType = PTPanTool.classForCoder()
      return
    }

    annotationPageNumber = UInt32(pageNumber)
    writePage = doc.getPage(annotationPageNumber)

    pdf.docUnlock()
  }

  func writeAnnotation() {
    print(#function)
  }
}

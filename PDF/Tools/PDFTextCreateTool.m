//
//  PDFTextCreateTool.m
//  linq
//
//  Created by Andrey Chevozerov on 02/07/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "PDFTextCreateTool.h"

@interface PDFTextCreateTool () <UIGestureRecognizerDelegate>

@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGPoint endPoint;

@property (nonatomic, assign) BOOL isRTL;
@property (nonatomic, assign) BOOL isDrag;
@property (nonatomic, assign) BOOL isAnnotationWritten;
@property (nonatomic, assign) BOOL isCreated;
@property (nonatomic, assign) BOOL isKeyboardOnScreen;

@property (nonatomic, strong) NSDate *touchesEndedTime;

@property (nonatomic, strong) PTPDFRect *writeRect;
@property (nonatomic, strong) PTPage *writePage;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UIScrollView *sv;

@end

@implementation PDFTextCreateTool

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl *)in_pdfViewCtrl
{
  self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
    self.isRTL = false;

    // use a temporary textView to see if the user is about to write in a RTL or LTR language
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectZero];
    [in_pdfViewCtrl addSubview:textView];
    [textView becomeFirstResponder];

    if ([NSLocale characterDirectionForLanguage:[textView textInputMode].primaryLanguage] == NSLocaleLanguageDirectionRightToLeft) {
      self.isRTL = true;
    }

    [textView resignFirstResponder];
    [textView removeFromSuperview];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (inputModeDidChange:)
                                                 name:UITextInputCurrentInputModeDidChangeNotification
                                               object:nil];
  }
  return self;
}

- (Class)annotClass
{
  return [PTFreeText class];
}

- (PTExtendedAnnotType)annotType
{
  return PTExtendedAnnotTypeFreeText;
}

- (BOOL)canEditStyle
{
  return YES;
}

+ (BOOL)createsAnnotation
{
  return YES;
}

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (void)setRectForFreeText:(PTFreeText *)freeText withRect:(PTPDFRect *)rect pdfViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl isRTL:(BOOL)isRTL
{
  // Get the annotation's content stream
  PTObj *contentStream = [[[freeText GetSDFObj] FindObj:@"AP"] FindObj:@"N"];

  // use element reader to iterate through elements and union their bounding boxes
  PTElementReader *er = [[PTElementReader alloc] init];

  PTObj *dict = [[PTObj alloc] init];

  PTContext *context = [[PTContext alloc] init];

  [er ReaderBeginWithSDFObj:contentStream resource_dict:dict ocg_context:context];

  PTElement *element;

  PTPDFRect *unionRect = 0;

  for (element = [er Next]; element != NULL; element = [er Next]) {
    PTPDFRect *elementRect = [element GetBBox];

    if ([element GetType] == e_pttext_obj) {
      if ([elementRect Width] && [elementRect Height]) {
        if (unionRect == 0) {
          unionRect = elementRect;
        }
        unionRect = [PTTool GetRectUnion:unionRect Rect2:elementRect];
      }
    }
  }

  double width = fabs([unionRect GetX2] - [unionRect GetX1]) + 10;
  double height = fabs([unionRect GetY2] - [unionRect GetY1]) + 10;

  PTRotate ctrlRotation = [pdfViewCtrl GetRotation];
  PTRotate pageRotation = [[freeText GetPage] GetRotation];
  int annotRotation = ((pageRotation + ctrlRotation) % 4) * 90;

  if (isRTL) {
    const int rightAligned = 2;

    [freeText SetQuaddingFormat:rightAligned];
    if (pageRotation == e_pt90 || pageRotation == e_pt270) {
      // Swap width and height if page is rotated 90 or 270 degrees
      width = width + height;
      height = width - height;
      width = width - height;
    }

    if (annotRotation == 0) {
      [unionRect SetX1:[rect GetX2] - width];
      [unionRect SetY1:[rect GetY1] - height];
      [unionRect SetX2:[rect GetX2]];
      [unionRect SetY2:[rect GetY1]];
    } else if (annotRotation == 90) {
      [unionRect SetX1:[rect GetX1]];
      [unionRect SetY1:[rect GetY2] - width];
      [unionRect SetX2:[rect GetX1] + height];
      [unionRect SetY2:[rect GetY2]];
    } else if (annotRotation == 180) {
      [unionRect SetX1:[rect GetX2] + width];
      [unionRect SetY1:[rect GetY1] + height];
      [unionRect SetX2:[rect GetX2]];
      [unionRect SetY2:[rect GetY1]];
    } else if (annotRotation == 270) {
      [unionRect SetX1:[rect GetX1] - height];
      [unionRect SetY1:[rect GetY2]];
      [unionRect SetX2:[rect GetX1]];
      [unionRect SetY2:[rect GetY2] + width];
    }
  } else {
    if (ctrlRotation == e_pt90 || ctrlRotation == e_pt270) {
      // Swap width and height if pdfViewCtrl is rotated 90 or 270 degrees
      width = width + height;
      height = width - height;
      width = width - height;
    }

    [unionRect SetX1:[rect GetX1]];
    [unionRect SetY1:[rect GetY1]];

    if (annotRotation == 0) {
      [unionRect SetX2:[rect GetX1] + width];
      [unionRect SetY2:[rect GetY1] - height];
    } else if (annotRotation == 90) {
      [unionRect SetX2:[rect GetX1] + width];
      [unionRect SetY2:[rect GetY1] + height];
    } else if (annotRotation == 180) {
      [unionRect SetX2:[rect GetX1] - width];
      [unionRect SetY2:[rect GetY1] + height];
    } else if (annotRotation == 270) {
      [unionRect SetX2:[rect GetX1] - width];
      [unionRect SetY2:[rect GetY1] - height];
    }
  }
  [unionRect Normalize];
  [freeText Resize:unionRect];
  [freeText SetRotation:annotRotation];
}

// MARK: - Private

- (void)inputModeDidChange:(NSNotification *)notification
{
  if (self.textView.text.length > 0) {
    return;
  }

  bool prior = self.isRTL;
  self.isRTL = ([NSLocale characterDirectionForLanguage:[self.textView textInputMode].primaryLanguage] == NSLocaleLanguageDirectionRightToLeft);

  if (prior != self.isRTL) {
    [self setTextAreaFrame];
  }
}

- (void)setUpTextEntry
{
  PTColorPostProcessMode mode = [self.pdfViewCtrl GetColorPostProcessMode];
  self.textView = [[UITextView alloc] init];
  self.sv = [[UIScrollView alloc] init];

  self.textView.contentInset = UIEdgeInsetsZero;
  self.textView.textContainerInset = UIEdgeInsetsZero;
  self.textView.textContainer.lineFragmentPadding = 0;

  self.sv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

  [self.sv addSubview:self.textView];
  [self.self addSubview:self.sv];

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector (keyboardWillShow:)
                                               name:UIKeyboardWillShowNotification
                                             object:nil];

  self.sv.backgroundColor = [UIColor clearColor];

  self.userInteractionEnabled = YES;
  self.sv.userInteractionEnabled = YES;
  self.textView.userInteractionEnabled = YES;
  self.textView.delegate = self;
  self.textView.textColor = [PTColorDefaults defaultColorForAnnotType:PTExtendedAnnotTypeFreeText attribute:ATTRIBUTE_TEXT_COLOR colorPostProcessMode:mode];
  self.textView.font = [UIFont fontWithName:@"Helvetica" size:[PTColorDefaults defaultFreeTextSize]*[self.pdfViewCtrl GetZoom]];
  self.textView.backgroundColor = [UIColor clearColor];

  // one day add background colour to live editing too?
  //http://stackoverflow.com/questions/15438869/uitextview-text-background-colour

  self.isCreated = NO;
  self.isAnnotationWritten = NO;

  return;
}

- (BOOL)setTextAreaFrame
{
  self.startPoint = self.longPressPoint;

  int pageNumber = [self.pdfViewCtrl GetPageNumberFromScreenPt: self.startPoint.x y: self.startPoint.y];
  if (pageNumber < 1) {
    return NO;
  }

  assert(self.startPoint.x > 0);
  assert(self.startPoint.y > 0);

  PTPDFRect *testRect = [[[self.pdfViewCtrl GetDoc] GetPage:pageNumber] GetCropBox];

  CGFloat cbx1 = [testRect GetX1];
  CGFloat cbx2 = [testRect GetX2];
  CGFloat cby1 = [testRect GetY1];
  CGFloat cby2 = [testRect GetY2];

  CGFloat pageLeft;
  CGFloat pageTop;

  [self ConvertPagePtToScreenPtX:&cbx1 Y:&cby1 PageNumber:pageNumber];
  [self ConvertPagePtToScreenPtX:&cbx2 Y:&cby2 PageNumber:pageNumber];

  double thickness = [PTColorDefaults defaultBorderThicknessForAnnotType:self.annotType];
  double offset = thickness * [self.pdfViewCtrl GetZoom];
  offset = 0;

  if (self.isRTL == false) {
    pageLeft = MIN(cbx1, cbx2);
    pageTop =  MIN(cby1, cby2);
    _endPoint.x = MAX(cbx1, cbx2);
    _endPoint.y = MAX(cby1, cby2);
    _startPoint.x = MAX(pageLeft, self.startPoint.x - offset);
    _startPoint.y = MAX(pageTop, self.startPoint.y - offset);
  } else {
    pageLeft = MIN(cbx1, cbx2);
    pageTop =  MIN(cby1, cby2);

    _endPoint.x = self.startPoint.x + offset;
    _endPoint.y = MAX(cby1, cby2);

    _startPoint.x = pageLeft;
    _startPoint.y = MAX(pageTop, self.startPoint.y + offset);
  }
  self.frame = CGRectMake([self.pdfViewCtrl GetHScrollPos] + self.startPoint.x,
                          [self.pdfViewCtrl GetVScrollPos] + self.startPoint.y,
                          fabs(self.endPoint.x - self.startPoint.x),
                          fabs(self.endPoint.y - self.startPoint.y));
  return YES;
}

- (BOOL)createEntryAtPoint:(CGPoint)point
{
  assert([NSThread isMainThread]);
  [self.superview bringSubviewToFront:self];
  self.startPoint = point;
  self.longPressPoint = point;
  BOOL couldSetFrame = [self setTextAreaFrame];

  if (!couldSetFrame) {
    self.nextToolType = [PTPanTool class];
    return NO;
  }

  if (self.textView == 0 && self.sv == 0) {
    [self setUpTextEntry];
  }

  [self activateTextEntry];
  return YES;
}

- (void)activateTextEntry
{
  assert([NSThread isMainThread]);

  CGRect cursorRect = [self.textView caretRectForPosition: self.textView.selectedTextRange.start];
  double thickness = [PTColorDefaults defaultBorderThicknessForAnnotType: self.annotType] * [self.pdfViewCtrl GetZoom];
  double inset = thickness * 2;
  double topOffset = self.textView.font.lineHeight - self.textView.font.pointSize;
  self.textView.textContainerInset = UIEdgeInsetsMake(inset - topOffset, inset, inset, inset);
  //Move and resize the frame by the width of the border (and the additional padding inside the border)
  self.sv.frame = CGRectMake(-inset, -inset - cursorRect.size.height / 2.0, self.frame.size.width-inset, self.frame.size.height - inset - cursorRect.size.height / 2.0);
  self.textView.frame = CGRectMake(0.0, 0.0, self.sv.frame.size.width, self.sv.frame.size.height);

  if ([self.textView canBecomeFirstResponder]) {
    [self.textView becomeFirstResponder];
  }

  self.isCreated = YES;

  UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTap:)];
  tgr.delegate = self;
  [tgr setCancelsTouchesInView:YES];
  [tgr setDelaysTouchesEnded:NO];
  [self.textView addGestureRecognizer:tgr];

  PTPDFDoc *doc = [self.pdfViewCtrl GetDoc];

  [self.pdfViewCtrl DocLock:YES];

  @try {
    int pageNumber = [self.pdfViewCtrl GetPageNumberFromScreenPt: self.startPoint.x y: self.startPoint.y];
    if (pageNumber < 1) {
      pageNumber = [self.pdfViewCtrl GetPageNumberFromScreenPt: self.endPoint.x y: self.endPoint.y];
    }

    if (pageNumber < 1) {
      self.nextToolType = [PTPanTool class];
      return;
    }

    self.annotationPageNumber = pageNumber;
    self.writePage = [doc GetPage: pageNumber];
  }
  @catch (NSException *exception) {
    printf("activateTextEntry Exception: %s: %s\n", exception.name.UTF8String, exception.reason.UTF8String);
  }
  @finally {
    [self.pdfViewCtrl DocUnlock];
  }
}

- (void)writeAnnotation
{
  if (self.isAnnotationWritten || !self.textView || [self.textView.text isEqualToString:@""]) {
    return;
  }
  PTPDFDoc *doc = [self.pdfViewCtrl GetDoc];
  PTFreeText *annotation;

  @try
  {
    [self.pdfViewCtrl DocLock:YES];

    CGRect screenRect = [self.pdfViewCtrl convertRect:self.textView.frame fromView:self.textView.superview];
    CGFloat x1 = screenRect.origin.x;
    CGFloat y1 = screenRect.origin.y;
    CGFloat x2 = screenRect.origin.x+screenRect.size.width;
    CGFloat y2 = screenRect.origin.y+screenRect.size.height;

    [self ConvertScreenPtToPagePtX:&x1 Y:&y1 PageNumber:self.annotationPageNumber];
    [self ConvertScreenPtToPagePtX:&x2 Y:&y2 PageNumber:self.annotationPageNumber];

    self.writeRect = [[PTPDFRect alloc] initWithX1:x1 y1:y1 x2:x2 y2:y2];

    annotation = [PTFreeText Create:(PTSDFDoc *)doc pos:self.writeRect];
    [annotation SetFontSize: [PTColorDefaults defaultFreeTextSize]];
    [annotation SetContents: self.textView.text];

    if (self.annotationAuthor && self.annotationAuthor.length > 0 && [annotation isKindOfClass:[PTMarkup class]]) {
      [(PTMarkup *)annotation SetTitle:self.annotationAuthor];
    }

    // write text colour
    PTColorPt *cp = [PTColorDefaults defaultColorPtForAnnotType:self.annotType attribute:ATTRIBUTE_TEXT_COLOR  colorPostProcessMode:e_ptpostprocess_none];
    int numCompsText = [PTColorDefaults numCompsInColorPtForAnnotType:self.annotType attribute:ATTRIBUTE_TEXT_COLOR];
    if (numCompsText > 0) {
      [annotation SetTextColor:cp col_comp:3];
    } else {
      [annotation SetTextColor:cp col_comp:0];
    }

    PTColorPt *strokeColor = [PTColorDefaults defaultColorPtForAnnotType:self.annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:e_ptpostprocess_none];
    int numCompsStroke = [PTColorDefaults numCompsInColorPtForAnnotType:self.annotType attribute:ATTRIBUTE_STROKE_COLOR];
    [annotation SetLineColor:strokeColor col_comp:numCompsStroke];

    // write border colour
    double thickness = [PTColorDefaults defaultBorderThicknessForAnnotType:self.annotType];
    PTBorderStyle *bs = [annotation GetBorderStyle];

    if (numCompsStroke == 0) {
      thickness = 0.0;
    }

    [bs SetWidth:thickness];
    [annotation SetBorderStyle:bs oldStyleOnly:NO];

    // push back annotation now in case of rotated page
    [self.writePage AnnotPushBack:annotation];
    [annotation RefreshAppearance];

    [PTFreeTextCreate setRectForFreeText:annotation withRect:self.writeRect pdfViewCtrl:self.pdfViewCtrl isRTL:self.isRTL];

    [annotation RefreshAppearance];

    // write fill colour
    int numCompsFill = [PTColorDefaults numCompsInColorPtForAnnotType:PTExtendedAnnotTypeFreeText attribute:ATTRIBUTE_FILL_COLOR];
    cp = [PTColorDefaults defaultColorPtForAnnotType:PTExtendedAnnotTypeFreeText attribute:ATTRIBUTE_FILL_COLOR colorPostProcessMode:e_ptpostprocess_none];

    if (numCompsFill > 0) {
      [annotation SetColor:cp numcomp:3];
    } else {
      [annotation SetColor:cp numcomp:0];
    }
    [annotation RefreshAppearance];
    self.isAnnotationWritten = YES;
  }
  @catch (NSException *exception) {
    NSLog(@"Exception: %@: %@",exception.name, exception.reason);
  }
  @finally {
    [self.pdfViewCtrl DocUnlock];
  }

  if (self.annotationPageNumber > 0) {
    [self.pdfViewCtrl UpdateWithAnnot:annotation page_num:self.annotationPageNumber];
  }

  [self annotationAdded:annotation onPageNumber: self.annotationPageNumber];

  [self.textView removeFromSuperview];
  self.textView = nil;
  [self.sv removeFromSuperview];
  self.sv = nil;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
  self.isDrag = NO;
  [self writeAnnotation];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  return YES;
}

- (BOOL)onSwitchToolEvent:(id)userData
{
  NSString *start = (NSString *)userData;

  if (userData && ((![start isEqualToString:@"Start"] && self.backToPanToolAfterUse) || (self.backToPanToolAfterUse && [start isEqualToString:@"BackToPan"]))) {
    self.nextToolType = [PTPanTool class];
    return NO;
  } else if ([start isEqualToString:@"BackToPan"] && !self.backToPanToolAfterUse) {
    self.nextToolType = [PTPanTool class];
    return NO;
  } else if ([start isEqualToString:@"CloseAnnotationToolbar"] && !self.backToPanToolAfterUse) {
    return NO;
  } else {
    self.isCreated = NO;
    self.startPoint = self.longPressPoint;

    [self setTextAreaFrame];
    [self setUpTextEntry];
    [self activateTextEntry];

    return YES;
  }
}

// MARK: - Keyboard events

- (void)keyboardWillShow:(NSNotification *)notification
{
  CGRect cursorRect = [self.textView caretRectForPosition: self.textView.selectedTextRange.start];
  CGRect cursorScreenRect = [self.pdfViewCtrl convertRect: cursorRect fromView: self.textView];
  CGFloat topEdge = 0.0;
  if (@available(iOS 11.0, *)) {
    topEdge = self.pdfViewCtrl.safeAreaInsets.top;
  }

  [self.pdfViewCtrl keyboardWillShow:notification rectToNotOverlapWith:cursorScreenRect topEdge:topEdge];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector (keyboardWillHide:)
                                               name: UIKeyboardWillHideNotification
                                             object:nil];
  self.isKeyboardOnScreen = true;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
  self.isCreated = NO;
  self.isKeyboardOnScreen = false;
  [self.pdfViewCtrl keyboardWillHide:notification];
  [self.toolManager createSwitchToolEvent:@"BackToPan"];
}

// MARK: - Interaction events

- (BOOL)pdfViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl handleTap:(UITapGestureRecognizer *)sender
{
  if (self.isCreated == NO) {
    self.isDrag = NO;
    [self createEntryAtPoint:[sender locationOfTouch:0 inView:self.pdfViewCtrl]];
    return YES;
  }

  if (self.textView != nil) {
    // UI gesture recognizer may also come in after the touches ended. This is to prevent
    // one from firing after the other, causing the keyboard to popup and then immediately
    // dismiss.
    if (self.isDrag && [[NSDate date] timeIntervalSinceDate:self.touchesEndedTime] < 0.85) {
      return YES;
    }

    [self.textView resignFirstResponder];
    self.frame = CGRectZero;
  }

  if (self.backToPanToolAfterUse) {
    self.nextToolType = [PTPanTool class];
    return NO;
  } else {
    self.isCreated = NO;
    return YES;
  }
}

- (void)textTap:(UITapGestureRecognizer *)gestureRecognizer
{
  CGSize textSize = [self.textView.text boundingRectWithSize:self.textView.bounds.size
                                                     options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                  attributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:[PTColorDefaults defaultFreeTextSize] * [self.pdfViewCtrl GetZoom]] }
                                                     context:nil].size;

  CGPoint down = [gestureRecognizer locationInView: self.textView];

  // 6 for textview inset
  if (down.x - 6.0 < textSize.width && down.y - 6.0 < textSize.height) {
    // tap was on text so ignore
  } else {
    // tap was outside of text, dismiss textview and commit annotation
    [self pdfViewCtrl:self.pdfViewCtrl handleTap:gestureRecognizer];
  }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  // required for the textview gesture recognizer to fire
  return YES;
}

- (BOOL)pdfViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl touchesShouldCancelInContentView:(UIView *)view
{
  return [super pdfViewCtrl:pdfViewCtrl touchesShouldCancelInContentView: view];
}

- (BOOL)pdfViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
  self.isDrag = YES;
  self.touchesEndedTime = [NSDate date];
  return YES;
}

- (BOOL)pdfViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  if (!self.isDrag) {
    return YES;
  }

  UITouch *touch = touches.allObjects[0];

  if (self.isCreated == NO) {
    [self createEntryAtPoint:[touch locationInView:self.pdfViewCtrl]];
  }

  self.isCreated = YES;

  if (self.backToPanToolAfterUse) {
    self.nextToolType = [PTPanTool class];
    return NO;
  }
  return YES;
}

- (BOOL)pdfViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
  return YES;
}

@end

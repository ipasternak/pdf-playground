//
//  PDFTextCreateTool.h
//  linq
//
//  Created by Andrey Chevozerov on 02/07/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Tools/Tools.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates a free text annotation.
 */
@interface PDFTextCreateTool : PTTool <UITextViewDelegate>

/**
 Sets the rect for a `PTFreeText` annotation.

 @param freeText The `PTFreeText` object.
 @param rect The `PTPDFRect` representation of the associated `UITextView`.
 @param pdfViewCtrl The `PTPDFViewCtrl` object.
 @param isRTL A `BOOL` indicating whether the text is in a right-to-left language.
 */
+(void)setRectForFreeText:(PTFreeText*)freeText withRect:(PTPDFRect*)rect pdfViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl isRTL:(BOOL)isRTL;

@end

NS_ASSUME_NONNULL_END

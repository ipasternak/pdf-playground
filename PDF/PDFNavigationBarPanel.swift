import UIKit

final class PDFNavigationBarPanel {
  enum Action {
    case back
    case done
    case markup
    case chooseBinder
    case undo
    case redo
    case search
    case recent
    case bookmark
    case viewMode
    case more
  }

  enum Mode {
    case normal(isBookmarked: Bool)
    case markup(canUndo: Bool, canRedo: Bool)
  }

  private let navigationItem: UINavigationItem
  private let actionHandler: (Action, UIBarButtonItem?) -> Void
  private (set) var mode: Mode = .normal(isBookmarked: false)

  init(navigationItem: UINavigationItem, didTapAction: @escaping (Action, UIBarButtonItem?) -> Void) {
    self.navigationItem = navigationItem
    self.actionHandler = didTapAction
    updateButtons()
  }

  func changeMode(_ mode: Mode) {
    self.mode = mode
    updateButtons()
  }
}

private extension PDFNavigationBarPanel {
  @objc func didTapBack() {
    actionHandler(.back, nil)
  }

  @objc func didTapDone() {
    actionHandler(.done, nil)
  }

  @objc func didTapMarkup() {
    actionHandler(.markup, nil)
  }

  @objc func didTapUndo() {
    actionHandler(.undo, nil)
  }

  @objc func didTapRedo() {
    actionHandler(.redo, nil)
  }

  @objc func didTapSearch(_ sender: UIBarButtonItem) {
    actionHandler(.search, sender)
  }

  @objc func didTapRecent() {
    actionHandler(.recent, nil)
  }

  @objc func didTapBookmark() {
    actionHandler(.bookmark, nil)
  }

  @objc func didTapViewMode() {
    actionHandler(.viewMode, nil)
  }

  @objc func didTapMore() {
    actionHandler(.more, nil)
  }

  func updateButtons() {
    navigationItem.setLeftBarButton(makeLeftItem(), animated: true)
    navigationItem.setRightBarButtonItems(makeRightItems(), animated: true)
  }

  func makeLeftItem() -> UIBarButtonItem {
    let item: UIBarButtonItem
    switch mode {
    case .normal:
      item = .init(image: image(for: .back), style: .plain, target: self, action: #selector(didTapBack))
    case .markup:
      item = .init(barButtonSystemItem: .done, target: self, action: #selector(didTapDone))
    }
    item.tintColor = UIColor.black
    return item
  }

  func makeRightItems() -> [UIBarButtonItem] {
    var items: [UIBarButtonItem] = []
    switch mode {
    case .normal:
      items = [
        UIBarButtonItem(image: image(for: .more), style: .plain, target: self, action: #selector(didTapMore)),
//        UIBarButtonItem(image: image(for: .viewMode), style: .plain, target: self, action: #selector(didTapViewMode)),
        UIBarButtonItem(image: image(for: .bookmark), style: .plain, target: self, action: #selector(didTapBookmark)),
        UIBarButtonItem(image: image(for: .recent), style: .plain, target: self, action: #selector(didTapRecent)),
        UIBarButtonItem(image: image(for: .search), style: .plain, target: self, action: #selector(didTapSearch(_:))),
        UIBarButtonItem(image: image(for: .markup), style: .plain, target: self, action: #selector(didTapMarkup))
      ]
    case .markup:
      items = [
        // TODO: Hidden according to LIMOB-172
//        makeRedoItem(),
//        makeUndoItem()
      ]
    }
    items.forEach { $0.tintColor = UIColor.black }
    return items
  }

  func makeUndoItem() -> UIBarButtonItem {
    let item = UIBarButtonItem(image: image(for: .undo), style: .plain, target: self, action: #selector(didTapUndo))
    if case .markup(false, _) = mode {
      item.isEnabled = false
    }
    return item
  }

  func makeRedoItem() -> UIBarButtonItem {
    let item = UIBarButtonItem(image: image(for: .redo), style: .plain, target: self, action: #selector(didTapRedo))
    if case .markup(_, false) = mode {
      item.isEnabled = false
    }
    return item
  }

  func image(for action: Action) -> UIImage? {
    switch action {
    case .back:         return UIImage(named: "BackIcon")
    case .done:         return nil
    case .markup:       return UIImage(named: "AnnotationsIcon")
    case .chooseBinder: return nil
    case .undo:
      if case let .markup(canUndo, _) = mode {
        return UIImage(named: canUndo ? "UndoEnabledIcon" : "UndoDisabledIcon")
      } else {
        return nil
      }
    case .redo:
      if case let .markup(_, canRedo) = mode {
        return UIImage(named: canRedo ? "RedoEnabledIcon" : "RedoDisabledIcon")
      } else {
        return nil
      }
    case .search:       return UIImage(named: "SearchIcon")
    case .recent:       return UIImage(named: "RecentlyViewedIcon")
    case .bookmark:
      if case .normal(true) = mode {
        return UIImage(named: "BookmarkIconFilled")
      } else {
        return UIImage(named: "BookmarkIcon")
      }
    case .viewMode:     return UIImage(named: "ViewModeIcon")
    case .more:         return UIImage(named: "MoreIcon")
    }
  }
}
